This simple application is written in rust that allows you to upload images and resize them.

## Usage

An image can be sent in three ways.

* multipart/form-data request to `/upload` endpoint. This method allow send multiple file at once
* POST request to `/upload` endpoint with the json and image encoded to base64 `{"data": "<base64 image data>"}`
* GET request to `/upload` endpoint with the argument `url` which is a link to the image

Server accepts requests on port 8080

## Docker

You also can run this application in docker. To do this simple execute

`docker-compose up -d`

This command will build the container with the application and run it.
