FROM rust as builder

WORKDIR /usr/src/app

COPY . .

RUN cargo build --release

FROM debian:stretch

EXPOSE 8080

RUN apt-get update && apt-get install -y --no-install-recommends \
		ca-certificates \
		libssl1.1 \
		netbase \
	&& rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/src/app/target/release/file_uploader /bin/

WORKDIR /data
VOLUME /data

CMD ["/bin/file_uploader"]
