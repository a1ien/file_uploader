#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

use std::{io, sync::mpsc};

use crate::image_resize::Message;
use actix_web::{guard, web, App, HttpServer};

mod api;
mod image_resize;

fn main() -> io::Result<()> {
    std::env::set_var(
        "RUST_LOG",
        "actix_server=debug,actix_web=debug,file_uploader=trace",
    );
    env_logger::init();

    let (tx, rx) = mpsc::channel();
    let resizer_thread = image_resize::start_resizer(rx);
    let app_tx = tx.clone();
    let app = move || {
        debug!("Constructing the App");

        App::new().data(app_tx.clone()).service(
            web::resource("/upload")
                .route(
                    web::post()
                        .guard(guard::Header("content-type", "application/json"))
                        .to(api::upload_post)
                        .data(web::JsonConfig::default().limit(2_097_152)),
                )
                .route(web::post().to_async(api::upload_multipart))
                .route(web::get().to_async(api::upload_url)),
        )
    };

    info!("Starting server");
    HttpServer::new(app).bind("0.0.0.0:8080")?.run()?;
    tx.send(Message::Exit).expect("Can't send exit message");
    resizer_thread.join().expect("Can't join");
    info!("Shutdown");
    Ok(())
}
