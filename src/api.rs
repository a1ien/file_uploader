use std::sync::mpsc;

use crate::image_resize::Message;
use actix_multipart::{Item, Multipart};
use actix_web::{client::Client, error, http::StatusCode, web, Error, HttpResponse, Result};
use bytes::Bytes;
use futures::{future::ok, Future, Stream};
use serde_json::json;

#[derive(Deserialize, Debug)]
pub struct ImageUrl {
    url: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct JsonData {
    data: Bytes,
}

pub fn upload_multipart(
    tx: web::Data<mpsc::Sender<Message>>,
    multipart: Multipart,
) -> impl Future<Item = HttpResponse, Error = Error> {
    debug!("upload_multipart");

    multipart
        .map_err(error::ErrorInternalServerError)
        .map(handle_multipart_item)
        .flatten()
        .collect()
        .map(move |files| {
            HttpResponse::Ok().json(
                files
                    .iter()
                    .cloned()
                    .map(|v| resize_image(tx.get_ref(), v))
                    .collect::<serde_json::Value>(),
            )
        })
}

fn handle_multipart_item(item: Item) -> Box<Stream<Item = Bytes, Error = Error>> {
    match item {
        Item::Field(field) => Box::new(field.concat2().from_err().into_stream()),
        Item::Nested(mp) => Box::new(
            mp.map_err(error::ErrorInternalServerError)
                .map(handle_multipart_item)
                .flatten(),
        ),
    }
}

pub fn upload_post(
    tx: web::Data<mpsc::Sender<Message>>,
    data: web::Json<JsonData>,
) -> Result<HttpResponse> {
    debug!("upload_post");
    let data = base64::decode(&data.data).map_err(error::ErrorBadRequest)?;
    Ok(HttpResponse::Ok().json(resize_image(tx.get_ref(), data.into())))
}

pub fn upload_url(
    tx: web::Data<mpsc::Sender<Message>>,
    image: web::Query<ImageUrl>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    debug!("upload_url {:?}", &image);

    Client::new()
        .get(&image.url)
        .send()
        .map_err(error::ErrorBadRequest)
        .and_then(|mut resp| {
            resp.body()
                .from_err()
                .and_then(move |data| match resp.status() {
                    StatusCode::OK => ok(HttpResponse::Ok().json(resize_image(tx.get_ref(), data))),
                    _ => ok(HttpResponse::BadRequest().json(json!({"status" :  "err" }))),
                })
        })
}

fn resize_image(tx: &mpsc::Sender<Message>, data: Bytes) -> serde_json::Value {
    json!({"status" : if tx.send(Message::Data(data)).is_ok() { "ok" } else { "err" }})
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_http::HttpService;
    use actix_http_test::TestServer;
    use actix_web::App;

    #[test]
    fn test_url() {
        let (tx, rx) = mpsc::channel::<Message>();
        let mut srv = TestServer::new(move || {
            HttpService::new(
                App::new()
                    .data(tx.clone())
                    .service(web::resource("/upload").to_async(upload_url)),
            )
        });
        let req = srv.get("/upload?url=https://www.rust-lang.org/static/images/rust-social.jpg");

        let mut resp = srv.block_on(req.send()).unwrap();
        assert_eq!(resp.status(), StatusCode::OK);
        let bytes = srv.block_on(resp.body()).unwrap();

        assert_eq!(
            json!({"status" : "ok"}),
            serde_json::from_str::<serde_json::Value>(std::str::from_utf8(&bytes).unwrap())
                .unwrap()
        );

        let data = match rx.recv().unwrap() {
            Message::Data(data) => data,
            _ => unreachable!(),
        };
        let img = image::guess_format(&data).unwrap();
        assert_eq!(img, image::JPEG);
    }

    #[test]
    fn test_wrong_url() {
        let (tx, _rx) = mpsc::channel::<Message>();
        let mut srv = TestServer::new(move || {
            HttpService::new(
                App::new()
                    .data(tx.clone())
                    .service(web::resource("/upload").to_async(upload_url)),
            )
        });
        let req = srv.get("/upload?url=rust-social.jpg");
        let response = srv.block_on(req.send()).unwrap();

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }
}
