use std::{
    fs::{self, File},
    io::{self, Write},
    path::{Path, PathBuf},
    sync::mpsc,
    thread::{self, JoinHandle},
};

use bytes::Bytes;
use chrono::Utc;

const IMG_SIZE: u32 = 100;
pub const TMP_DIR: &str = "/tmp";

pub enum Message {
    Data(Bytes),
    Exit,
}

pub fn start_resizer(rx: mpsc::Receiver<Message>) -> JoinHandle<()> {
    thread::spawn(move || {
        info!("Starting resizer thread");
        let folder = get_folder();
        while let Ok(data) = rx.recv() {
            match data {
                Message::Data(data) => {
                    match image_resize(&data, (IMG_SIZE, IMG_SIZE)) {
                        Ok((img, fmt)) => match save_image(&img, &folder, fmt) {
                            Ok(_) => debug!("Image saved"),
                            Err(e) => error!("Can't save image. Error {:?}", e),
                        },
                        Err(e) => error!("Can't resize image. Error {:?}", e),
                    };
                }
                Message::Exit => break,
            }
        }
        info!("Resizer thread stopped");
    })
}

fn image_resize(
    buf: &[u8],
    size: (u32, u32),
) -> Result<(Vec<u8>, image::ImageFormat), image::ImageError> {
    let mut result: Vec<u8> = Vec::new();
    let format = image::guess_format(buf)?;
    let img = image::load_from_memory_with_format(&buf, format)?;
    img.resize(size.0, size.1, image::FilterType::Lanczos3)
        .write_to(&mut result, format)?;
    debug!("Image resized");
    Ok((result, format))
}

fn get_ext(fmt: image::ImageFormat) -> &'static str {
    match fmt {
        image::PNG => "png",
        image::JPEG => "jpg",
        image::GIF => "gif",
        image::WEBP => "webp",
        image::PNM => "pnm",
        image::BMP => "bmp",
        image::ICO => "ico",
        _ => "unsupported",
    }
}

fn save_image(img: &[u8], path: impl AsRef<Path>, fmt: image::ImageFormat) -> io::Result<()> {
    let name = format!("{:x}", &md5::compute(img));
    let mut file_name = PathBuf::from(path.as_ref());
    file_name.push(name);
    file_name.set_extension(get_ext(fmt));
    let mut f = File::create(file_name)?;
    f.write_all(&img)?;
    Ok(())
}

fn create_dir<P: AsRef<Path>>(path: P) -> io::Result<()> {
    match fs::create_dir_all(&path) {
        Ok(_) => info!("Directory {} created", path.as_ref().display()),
        Err(e) => match e.kind() {
            io::ErrorKind::AlreadyExists => {}
            _ => return Err(e),
        },
    };
    Ok(())
}

fn get_folder() -> PathBuf {
    let date = Utc::now();
    let mut path = std::env::current_dir().unwrap_or_else(|_| PathBuf::from(TMP_DIR));
    path.push(date.format("%Y_%m_%d").to_string());
    match create_dir(&path) {
        Ok(_) => path,
        Err(e) => {
            error!("Can't create path {} with error {:?}", path.display(), e);
            PathBuf::from(TMP_DIR)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use image::GenericImageView;
    use std::io::Read;

    #[test]
    fn image_resizer() {
        let expect_size = (IMG_SIZE, IMG_SIZE);
        let mut buf = vec![];
        File::open("static/rust.jpg")
            .unwrap()
            .read_to_end(&mut buf)
            .unwrap();
        let img = image_resize(&buf, (IMG_SIZE, IMG_SIZE)).unwrap();
        let img = image::load_from_memory(&img.0).unwrap();
        assert_eq!(expect_size, (img.width(), img.height()));
    }
}
