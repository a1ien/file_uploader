#!/usr/bin/env python3

import requests
import base64

url = "http://127.0.0.1:8080/upload"


def test_json():
    f = open("static/rust.jpg", "rb")

    payload = "{\"data\": \"" + base64.b64encode(f.read()).decode('ascii') + "\"}"
    headers = {'content-type': 'application/json'}
    response = requests.request("POST", url, data=payload, headers=headers)

    if response.text != '{"status":"ok"}':
        print("Json Fail")
        exit(1)
    print("Json Ok")


def test_multipart():

    files = {'media': open("static/rust.jpg", "rb")}
    response = requests.post(url, files=files)
    if response.text != '[{"status":"ok"}]':
        print("Multipart Fail")
        exit(1)
    print("Multipart Ok")


def test_url():

    querystring = {"url": "https://www.rust-lang.org/static/images/rust-social.jpg"}

    response = requests.request("GET", url, params=querystring)
    if response.text != '{"status":"ok"}':
        print("Url Fail")
        exit(1)
    print("Url Ok")


test_json()
test_multipart()
test_url()
